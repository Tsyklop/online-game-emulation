package info.tsyklop.onlinegameemulator.model;

public class RestResponse {

    private final String message;

    public RestResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public static RestResponse with(String message) {
        return new RestResponse(message);
    }

}
