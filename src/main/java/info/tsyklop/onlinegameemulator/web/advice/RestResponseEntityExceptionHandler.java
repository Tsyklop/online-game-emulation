package info.tsyklop.onlinegameemulator.web.advice;

import info.tsyklop.onlinegameemulator.exception.RestApiException;
import info.tsyklop.onlinegameemulator.exception.RestValidationException;
import info.tsyklop.onlinegameemulator.model.RestFieldError;
import info.tsyklop.onlinegameemulator.model.RestResponse;
import info.tsyklop.onlinegameemulator.model.RestValidationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return this.handleExceptionInternal(ex, RestResponse.with("Message Not Readable"), headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return this.handleExceptionInternal(ex, RestValidationResponse.with(buildFieldsErrors(ex.getAllErrors())), headers, status, request);
    }

    @ExceptionHandler(RestApiException.class)
    protected ResponseEntity<Object> handleRestApiException(RestApiException ex, WebRequest request) {
        LOGGER.error("REST API ERROR", ex);
        return ResponseEntity.badRequest().body(RestResponse.with(ex.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.status(status).headers(headers).body(Objects.isNull(body) ? RestResponse.with(ex.getMessage()) : body);
    }

    @ExceptionHandler(RestValidationException.class)
    protected ResponseEntity<Object> handleRestValidationException(RestValidationException ex, WebRequest request) {
        return ResponseEntity
                .badRequest()
                .body(
                        RestValidationResponse.with(
                                "Validation Error",
                                buildFieldsErrors(ex.getErrors().getAllErrors())
                        )
                );
    }

    private List<RestFieldError> buildFieldsErrors(List<ObjectError> errors) {
        return errors
                .stream()
                .map(objectError -> (FieldError) objectError)
                .map(fieldError -> new RestFieldError(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
    }

}
