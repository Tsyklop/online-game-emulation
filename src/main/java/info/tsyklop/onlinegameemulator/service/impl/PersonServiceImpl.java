package info.tsyklop.onlinegameemulator.service.impl;

import info.tsyklop.onlinegameemulator.config.LevelExperienceConfig;
import info.tsyklop.onlinegameemulator.exception.RestApiException;
import info.tsyklop.onlinegameemulator.model.RestResponse;
import info.tsyklop.onlinegameemulator.model.dto.PersonInfo;
import info.tsyklop.onlinegameemulator.persistence.PersonRepository;
import info.tsyklop.onlinegameemulator.service.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    private final LevelExperienceConfig levelExperienceConfig;

    @Override
    public ResponseEntity<PersonInfo> getPersonInfo(Integer id) {

        Optional<PersonInfo> personInfo = this.personRepository.findById(id);

        if(!personInfo.isPresent()) {
            throw RestApiException.notFound("Person Not Found");
        }

        return ResponseEntity.ok(personInfo.get());
    }

    @Override
    public ResponseEntity<RestResponse> putExperience(Integer id, Integer experience) {

        Optional<PersonInfo> personInfo = this.personRepository.findById(id);

        if(!personInfo.isPresent()) {
            personInfo = Optional.of(this.personRepository.save(id, new PersonInfo()));
        }

        calculateLevel(personInfo.get(), experience);

        return ResponseEntity.ok(RestResponse.with("Experience updated!"));

    }

    private void calculateLevel(PersonInfo personInfo, Integer experience) {

        if(this.levelExperienceConfig.getMaxLevel() == personInfo.getLevel()) {
            LOGGER.info("PERSON HAVE MAX LEVEL");
            return;
        }

        while(personInfo.getLevel() < levelExperienceConfig.getMaxLevel()) {

            Optional<Integer> levelExperience = this.levelExperienceConfig.findExperienceForLevel(personInfo.getLevel());

            if(!levelExperience.isPresent()) {
                LOGGER.info("LEVEL {} EXPERIENCE NOT FOUND. CONTINUE", personInfo.getLevel());
                continue;
            }

            Integer leftExp = levelExperience.get() - personInfo.getExperience();

            if(leftExp - experience > 0) {
                LOGGER.info("ADD EXPERIENCE TO PERSON - {}", experience);
                personInfo.expUp(experience);
                experience = 0;
            } else if(leftExp - experience <= 0) {
                personInfo.levelUp();
                experience = experience - leftExp;
                LOGGER.info("LEVEL UP FOR PERSON. LEFT FREE EXPERIENCE {}", experience);
            }

            if(experience <= 0) {
                LOGGER.info("FREE EXPERIENCE IS OVER");
                break;
            }

        }

        if(personInfo.getLevel() == this.levelExperienceConfig.getMaxLevel()) {
            LOGGER.info("PERSON REACHED MAX LEVEL {}", this.levelExperienceConfig.getMaxLevel());
        }

    }

}
