package info.tsyklop.onlinegameemulator.model;

import java.util.List;

public class RestValidationResponse extends RestResponse {

    private final List<RestFieldError> fields;

    public RestValidationResponse(String message, List<RestFieldError> fields) {
        super(message);
        this.fields = fields;
    }

    public List<RestFieldError> getFields() {
        return fields;
    }

    public static RestValidationResponse with(List<RestFieldError> fields) {
        return with("Validation Error", fields);
    }

    public static RestValidationResponse with(String message, List<RestFieldError> fields) {
        return new RestValidationResponse(message, fields);
    }

}
