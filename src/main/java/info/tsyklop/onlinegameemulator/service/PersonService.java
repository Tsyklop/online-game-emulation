package info.tsyklop.onlinegameemulator.service;

import info.tsyklop.onlinegameemulator.model.RestResponse;
import info.tsyklop.onlinegameemulator.model.dto.PersonInfo;
import org.springframework.http.ResponseEntity;

public interface PersonService {

    ResponseEntity<PersonInfo> getPersonInfo(Integer id);
    ResponseEntity<RestResponse> putExperience(Integer id, Integer experience);

}
