package info.tsyklop.onlinegameemulator.persistence.impl;

import info.tsyklop.onlinegameemulator.model.dto.PersonInfo;
import info.tsyklop.onlinegameemulator.persistence.PersonRepository;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class PersonRepositoryImpl implements PersonRepository {

    private final Map<Integer, PersonInfo> personInfo = new ConcurrentHashMap<>();

    @Override
    public PersonInfo save(Integer id, PersonInfo personInfo) {
        this.personInfo.put(id, Objects.requireNonNull(personInfo));
        return personInfo;
    }

    @Override
    public Optional<PersonInfo> findById(Integer id) {
        return Optional.ofNullable(this.personInfo.get(id));
    }

}
