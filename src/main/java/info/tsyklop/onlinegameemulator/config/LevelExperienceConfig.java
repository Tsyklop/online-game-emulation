package info.tsyklop.onlinegameemulator.config;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

@Getter
@Configuration
public class LevelExperienceConfig {

    private Integer maxLevel = 0;

    private final Map<Integer, Integer> levelExperience = new HashMap<>();

    @PostConstruct
    public void postInit() throws IOException {
        Properties properties = PropertiesLoaderUtils.loadAllProperties("level-experience.properties");
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {

            String key = entry.getKey().toString();
            Integer value = Integer.parseInt(entry.getValue().toString());

            if (key.contains("-")) {

                List<String> parts = Arrays.stream(key.split("-"))
                        .filter(StringUtils::isNoneBlank)
                        .collect(Collectors.toList());

                if (parts.size() < 2) {
                    throw new RuntimeException(String.format("Key '%s' has incorrect format", key));
                }

                int from = Integer.parseInt(parts.get(0));
                int to = Integer.parseInt(parts.get(1));

                if (from > to) {
                    throw new RuntimeException(String.format("Key '%s' incorrect: from cannot be more than to value", key));
                }

                for (int i = from; i < (to + 1); i++) {
                    this.levelExperience.put(i, value);
                }

            } else {
                this.levelExperience.put(Integer.parseInt(key), value);
            }
        }

        this.levelExperience.keySet()
                .stream()
                .distinct()
                .max(Integer::compareTo)
                .ifPresent(integer -> this.maxLevel = integer);

    }

    public Optional<Integer> findExperienceForLevel(Integer level) {

        if (level.equals(this.maxLevel)) {
            return Optional.of(0);
        }

        while (level > 0) {

            Integer exp = this.levelExperience.get(level);

            if (Objects.isNull(exp)
                    || (exp <= 0 && !level.equals(maxLevel))) {
                --level;
                continue;
            }

            return Optional.of(exp);

        }

        return Optional.empty();

    }

}
