package info.tsyklop.onlinegameemulator.model.dto;

import lombok.Getter;

@Getter
public class PersonInfo {

    private int level = 1;

    private int experience = 0;

    public void levelUp() {
        this.level += 1;
        this.experience = 0;
    }

    public void expUp(Integer exp) {
        this.experience += exp;
    }

}
