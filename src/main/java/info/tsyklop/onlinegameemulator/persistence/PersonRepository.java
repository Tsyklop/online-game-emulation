package info.tsyklop.onlinegameemulator.persistence;

import info.tsyklop.onlinegameemulator.model.dto.PersonInfo;

import java.util.Optional;

public interface PersonRepository {
    PersonInfo save(Integer id, PersonInfo personInfo);
    Optional<PersonInfo> findById(Integer id);
}
