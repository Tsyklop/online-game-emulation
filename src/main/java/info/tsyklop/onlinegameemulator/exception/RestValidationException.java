package info.tsyklop.onlinegameemulator.exception;

import lombok.Getter;
import org.springframework.validation.Errors;

@Getter
public class RestValidationException extends RuntimeException {

    private final Errors errors;

    public RestValidationException(Errors errors) {
        this.errors = errors;
    }

    public RestValidationException(String message, Errors errors) {
        super(message);
        this.errors = errors;
    }

    public RestValidationException(String message, Throwable cause, Errors errors) {
        super(message, cause);
        this.errors = errors;
    }

    public RestValidationException(Throwable cause, Errors errors) {
        super(cause);
        this.errors = errors;
    }

    public RestValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Errors errors) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errors = errors;
    }

}
