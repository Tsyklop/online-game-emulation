package info.tsyklop.onlinegameemulator.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class RestApiException extends RuntimeException {

    private final HttpStatus status;

    private RestApiException(HttpStatus status) {
        super();
        this.status = status;
    }

    private RestApiException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public static RestApiException of(HttpStatus status) {
        return new RestApiException(status, null);
    }

    public static RestApiException of(HttpStatus status, String message) {
        return new RestApiException(status, message);
    }

    public static RestApiException conflict(String message) {
        return of(HttpStatus.CONFLICT, message);
    }

    public static RestApiException notFound(String message) {
        return of(HttpStatus.NOT_FOUND, message);
    }

    public static RestApiException badRequest(String message) {
        return of(HttpStatus.BAD_REQUEST, message);
    }

}
