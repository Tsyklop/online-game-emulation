package info.tsyklop.onlinegameemulator.model;

public class RestFieldError {

    private final String name;

    private final String message;

    public RestFieldError(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

}
