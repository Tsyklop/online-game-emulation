package info.tsyklop.onlinegameemulator.web;

import info.tsyklop.onlinegameemulator.model.RestResponse;
import info.tsyklop.onlinegameemulator.model.dto.PersonInfo;
import info.tsyklop.onlinegameemulator.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/person", consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class PersonRestController {

    private final PersonService personService;

    @GetMapping("/{id}")
    public ResponseEntity<PersonInfo> getPersonInfo(@PathVariable Integer id) {
        return this.personService.getPersonInfo(id);
    }

    @PutMapping("/{id}/exp/{experience}")
    public ResponseEntity<RestResponse> puExperience(@PathVariable Integer id,
                                                   @PathVariable Integer experience) {
        return this.personService.putExperience(id, experience);
    }

}
